/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import com.mongodb.MongoClientSettings;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoDatabase;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.PojoCodecProvider;

import static org.bson.codecs.configuration.CodecRegistries.fromProviders;
import static org.bson.codecs.configuration.CodecRegistries.fromRegistries;

/**
 *
 * @author Kamen
 */
public class Database {

    private static final MongoClient MONGOCLIENT;
    private static final MongoDatabase DB1;
    private static final MongoDatabase DB2;
    private static final CodecRegistry POJOCODECSREGISTRY;

    /**
     * Creation d'une instance unique pour la bd
     */
    static {
        POJOCODECSREGISTRY = createCodecRegistry();
        MongoClientSettings settings = MongoClientSettings.builder()
                .codecRegistry(POJOCODECSREGISTRY)
                .build();
        MONGOCLIENT = MongoClients.create(settings);
        // MONGOCLIENT = MongoClients.create("mongodb://localhost:27017");
        DB1 = MONGOCLIENT.getDatabase("bd1");
        DB2 = MONGOCLIENT.getDatabase("bd2");
    }

    public static MongoClient getMONGOCLIENT() {
        return MONGOCLIENT;
    }

    public static MongoDatabase getDB1() {
        return DB1;
    }

    public static MongoDatabase getDB2() {
        return DB2;
    }

    private static CodecRegistry createCodecRegistry() {
        if (POJOCODECSREGISTRY != null) {
            return POJOCODECSREGISTRY;
        }
        return fromRegistries(MongoClientSettings.getDefaultCodecRegistry(),
                fromProviders(PojoCodecProvider.builder().automatic(true).build()));
    }

}
