/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import classe.ServerMessage;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;
import models.Article;
import models.Client;
import models.Commande;
import models.DetailLivraison;
import models.LigneCommande;
import models.Model;
import org.bson.conversions.Bson;

/**
 *
 * @author Kameni Alex
 */
public class Utils {

    public static ExecutorService pool;

    public static void main(String[] args) {
        pool = Executors.newCachedThreadPool();
        int port = 9000;
        if (args.length >= 1) {
            port = Integer.parseInt(args[0]);
        }
        try (ServerSocket serverSocket = new ServerSocket(port)) {
            System.out.println("Server is listening on port " + port);

            while (true) {
                try {
                    Socket socket = serverSocket.accept();
                    // System.out.println("New client connected " + socket.getInetAddress());
                    //  = true;
                    /**
                     * @todo Ajouter un mot de passe de connexions
                     */
                    pool.execute(() -> {
                        if (!socket.isClosed()) {
                            try {
                                traiterServerMessage(socket);
                            } catch (IOException | ClassNotFoundException ex) {
                                Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
                                // flag = false;
                                try {
                                    socket.close();
                                } catch (IOException ex1) {
                                    Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex1);
                                }
                            }
                        }
                    });
                } catch (IOException ex) {
                    Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } catch (IOException ex) {
            System.out.println("Server exception: " + ex.getMessage());
        }
    }

    /**
     * Traiter les messages contenues dans soc
     *
     * @param soc
     * @return
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public static Object traiterServerMessage(Socket soc) throws IOException, ClassNotFoundException {
        // OutputStream output = soc.getOutputStream();
        // ObjectOutputStream obj = new ObjectOutputStream(output);
        ServerMessage sm = (ServerMessage) read(soc);
        switch (sm.getAction()) {
            case "save":
                // Object save = read(soc);
                write(soc, save(sm.getOther(), sm.getMessage()));
            case "get":
                // Object get = read(soc);
                write(soc, get(sm.getMessage(), (Bson) sm.getOther()));
            default:
                System.out.println("Unkwon Actions");
        }
        return null;
    }

    public static Object read(Socket socket) throws ClassNotFoundException, IOException {
        InputStream input = socket.getInputStream();
        Object object;
        ObjectInputStream obj = new ObjectInputStream(input);
        object = obj.readObject();
        return object;
    }

    public static void write(Socket socket, Object message) throws IOException {
        OutputStream output = socket.getOutputStream();
        ObjectOutputStream out = new ObjectOutputStream(output);
        out.writeObject(message);
        out.flush();
    }

    /**
     * Sauvegarde obj dans la table : <b>table</b>
     *
     * @param obj
     * @param table
     * @return
     */
    public static boolean save(Object obj, String table) {
        MongoDatabase db = database.Database.getDB1();
        Model model = (Model) obj;
        if (model.valid()) {
            switch (table) {
                case "article":
                    Article article = (Article) model;
                    MongoCollection<Article> articles = db.getCollection("article", Article.class);
                    articles.insertOne(article);
                    break;
                case "client":
                    Client client = (Client) model;
                    MongoCollection<Client> clients = db.getCollection("client", Client.class);
                    clients.insertOne(client);
                    break;
                case "commande":
                    Commande commande = (Commande) model;
                    MongoCollection<Commande> commandes = db.getCollection("commande", Commande.class);
                    commandes.insertOne(commande);
                    break;
                case "detaillivraison":
                    DetailLivraison detail = (DetailLivraison) model;
                    MongoCollection<DetailLivraison> details = db.getCollection("detaillivraison", DetailLivraison.class);
                    details.insertOne(detail);
                    break;
                case "lignecommande":
                    LigneCommande ligne = (LigneCommande) model;
                    MongoCollection<LigneCommande> lignes = db.getCollection("lignecommande", LigneCommande.class);
                    lignes.insertOne(ligne);
                    break;
                default:
                    return false;
            }
        }
        return true;
    }

    /**
     * Lire les donnee de la table : <b>table</b> sous les contraintes de
     * <h1>bson</h1>
     *
     * @param table
     * @param bson
     * @return
     */
    public static List get(String table, Bson bson) {
        MongoDatabase db = database.Database.getDB1();
        List res = new ArrayList();
        switch (table) {
            case "article":
                // Article article = (Article) model;
                MongoCollection<Article> articleCollection = db.getCollection("article", Article.class);
                //List<Article> articles = new ArrayList<>();
                articleCollection.find(bson).forEach(new ConsumerImpl(res));
                break;
            case "client":
                MongoCollection<Client> clientCollection = db.getCollection("client", Client.class);
                // List<Client> clients = new ArrayList<>();
                clientCollection.find(bson).forEach(new ConsumerImpl(res));
                break;
            case "commande":
                MongoCollection<Commande> commandeCollection = db.getCollection("commande", Commande.class);
                // List<Commande> commandes = new ArrayList<>();
                commandeCollection.find(bson).forEach(new ConsumerImpl(res));
                break;
            case "detaillivraison":
                MongoCollection<DetailLivraison> detailCollection = db.getCollection("detaillivraison", DetailLivraison.class);
                // List<DetailLivraison> details = new ArrayList<>();
                detailCollection.find(bson).forEach(new ConsumerImpl(res));
                break;
            case "lignecommande":
                MongoCollection<LigneCommande> ligneCollection = db.getCollection("lignecommande", LigneCommande.class);
                // List<LigneCommande> lignes = new ArrayList<>();
                ligneCollection.find(bson).forEach(new ConsumerImpl(res));
                break;
            default:
                return null;
        }
        return res;
    }

    private static class ConsumerImpl implements Consumer<Object> {

        private final List commandes;

        public ConsumerImpl(List commandes) {
            this.commandes = commandes;
        }

        @Override
        public void accept(Object t) {
            commandes.add(t);
        }
    }

//    private static class ConsumerArticle implements Consumer<Article> {
//
//        private final List<Article> articles;
//
//        public ConsumerArticle(List<Article> articles) {
//            this.articles = articles;
//        }
//
//        @Override
//        public void accept(Article t) {
//            articles.add(t);
//        }
//    }
//
//    private static class ConsumerClient implements Consumer<Client> {
//
//        private final List<Client> articles;
//
//        public ConsumerClient(List<Client> articles) {
//            this.articles = articles;
//        }
//
//        @Override
//        public void accept(Client t) {
//            articles.add(t);
//        }
//    }
}
