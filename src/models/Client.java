/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.io.Serializable;
import org.bson.types.ObjectId;

/**
 *
 * @author Kameni Alex
 */
public class Client extends Model implements Serializable {

    private static final long serialVersionUID = 3670079982654483072L;

    private ObjectId id;
    private Integer noClient;
    private String nomClient;
    private String noTelephone;

    public Client() {
    }

    public Client(ObjectId id, Integer noClient, String nomClient, String noTelephone) {
        this.id = id;
        this.noClient = noClient;
        this.nomClient = nomClient;
        this.noTelephone = noTelephone;
    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public Integer getNoClient() {
        return noClient;
    }

    public void setNoClient(Integer noClient) {
        this.noClient = noClient;
    }

    public String getNomClient() {
        return nomClient;
    }

    public void setNomClient(String nomClient) {
        this.nomClient = nomClient;
    }

    public String getNoTelephone() {
        return noTelephone;
    }

    public void setNoTelephone(String noTelephone) {
        this.noTelephone = noTelephone;
    }

}
