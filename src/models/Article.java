/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.io.Serializable;
import org.bson.types.ObjectId;

/**
 *
 * @author Kameni Alex
 */
public class Article extends Model implements Serializable {

    private static final long serialVersionUID = 3670079982654483072L;

    private ObjectId id;
    private Integer noArticle;
    private String description;
    private Float prixUnitaire;
    private Integer quantiteEnStock;

    public Article() {
    }

    public Article(ObjectId id, Integer noArticle, String description, Float prixUnitaire, Integer quantiteEnStock) {
        this.id = id;
        this.noArticle = noArticle;
        this.description = description;
        this.prixUnitaire = prixUnitaire;
        this.quantiteEnStock = quantiteEnStock;
    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public Integer getNoArticle() {
        return noArticle;
    }

    public void setNoArticle(Integer noArticle) {
        this.noArticle = noArticle;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Float getPrixUnitaire() {
        return prixUnitaire;
    }

    public void setPrixUnitaire(Float prixUnitaire) {
        this.prixUnitaire = prixUnitaire;
    }

    public Integer getQuantiteEnStock() {
        return quantiteEnStock;
    }

    public void setQuantiteEnStock(Integer quantiteEnStock) {
        this.quantiteEnStock = quantiteEnStock;
    }

    
    @Override
    public boolean valid() {
        return quantiteEnStock >= 0;
    }
}
