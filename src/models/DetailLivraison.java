/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import exception.UnknownValueException;
import java.io.Serializable;
import org.bson.types.ObjectId;

/**
 *
 * @author Kameni Alex
 */
public class DetailLivraison extends Model implements Serializable {

    private static final long serialVersionUID = 3670079982654483072L;

    private ObjectId id;
    private Integer noClient;
    private Integer noCommande;
    private Integer noArticle;
    private Integer quantiteLivree;

    public DetailLivraison() {
    }

    public DetailLivraison(ObjectId id, Integer noClient, Integer noCommande, Integer noArticle, Integer quantiteLivree) throws Exception {
        if (quantiteLivree <= 0) {
            throw new UnknownValueException("La quantite ne peut etre inferieure a zero");
        }
        this.id = id;
        this.noClient = noClient;
        this.noCommande = noCommande;
        this.noArticle = noArticle;
        this.quantiteLivree = quantiteLivree;
    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public Integer getNoClient() {
        return noClient;
    }

    public void setNoClient(Integer noClient) {
        this.noClient = noClient;
    }

    public Integer getNoCommande() {
        return noCommande;
    }

    public void setNoCommande(Integer noCommande) {
        this.noCommande = noCommande;
    }

    public Integer getNoArticle() {
        return noArticle;
    }

    public void setNoArticle(Integer noArticle) {
        this.noArticle = noArticle;
    }

    public Integer getQuantiteLivree() {
        return quantiteLivree;
    }

    public void setQuantiteLivree(Integer quantiteLivree) {
        this.quantiteLivree = quantiteLivree;
    }

    @Override
    public boolean valid() {
        return quantiteLivree > 0;
    }

}
