/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.io.Serializable;
import org.bson.types.ObjectId;

/**
 *
 * @author Kameni Alex
 */
public class LigneCommande extends Model implements Serializable {

    private static final long serialVersionUID = 3670079982654483072L;

    private ObjectId id;
    private Integer noCommande;
    private Integer noArticle;
    private Integer quantite;

    public LigneCommande() {
    }

    public LigneCommande(ObjectId id, Integer noCommande, Integer noArticle, Integer quantite) {
        this.id = id;
        this.noCommande = noCommande;
        this.noArticle = noArticle;
        this.quantite = quantite;
    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public Integer getNoCommande() {
        return noCommande;
    }

    public void setNoCommande(Integer noCommande) {
        this.noCommande = noCommande;
    }

    public Integer getNoArticle() {
        return noArticle;
    }

    public void setNoArticle(Integer noArticle) {
        this.noArticle = noArticle;
    }

    public Integer getQuantite() {
        return quantite;
    }

    public void setQuantite(Integer quantite) {
        this.quantite = quantite;
    }

    @Override
    public boolean valid() {
        return quantite > 0;
    }

}
