/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.io.Serializable;
import java.util.Date;
import org.bson.types.ObjectId;

/**
 *
 * @author Kameni Alex
 */
public class Commande extends Model implements Serializable {

    private static final long serialVersionUID = 3670079982654483072L;

    private ObjectId id;
    private Integer noCommande;
    private Date dateCommande;
    private Integer noClient;

    public Commande() {
    }

    public Commande(ObjectId id, Integer noCommande, Date dateCommande, Integer noClient) {
        this.id = id;
        this.noCommande = noCommande;
        this.dateCommande = dateCommande;
        this.noClient = noClient;
    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public Integer getNoCommande() {
        return noCommande;
    }

    public void setNoCommande(Integer noCommande) {
        this.noCommande = noCommande;
    }

    public Date getDateCommande() {
        return dateCommande;
    }

    public void setDateCommande(Date dateCommande) {
        this.dateCommande = dateCommande;
    }

    public Integer getNoClient() {
        return noClient;
    }

    public void setNoClient(Integer noClient) {
        this.noClient = noClient;
    }

}
