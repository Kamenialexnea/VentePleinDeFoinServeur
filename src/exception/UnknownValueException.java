/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exception;

/**
 *
 * @author Kameni Alex
 */
public class UnknownValueException extends Exception {

    public UnknownValueException() {
    }

    public UnknownValueException(String message) {
        super(message);
    }

}
