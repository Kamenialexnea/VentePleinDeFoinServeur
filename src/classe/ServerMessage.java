/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classe;

import java.io.Serializable;

/**
 *
 * @author Kamen
 */
public class ServerMessage implements Serializable {

    private static final long serialVersionUID = 3670079982654483072L;

    private String action;
    private Object other;
    private String message;

    public ServerMessage(String action, Object other) {
        this.action = action;
        this.other = other;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public Object getOther() {
        return other;
    }

    public void setOther(Object other) {
        this.other = other;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "ServerMessage{" + "action=" + action + '}';
    }

}
